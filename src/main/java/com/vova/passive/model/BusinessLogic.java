package com.vova.passive.model;

import java.util.ArrayList;

public class BusinessLogic implements Model {
	private Offers offers;
	
	public BusinessLogic(Offers offers) {
		this.offers = offers;
	}
	
	@Override
	public ArrayList<Shipping> getOffers() {
		return offers.getShippings();
	}

	@Override
	public Shipping getWithMaxPrice() {
		return offers.getWithMaxPrice();
	}

	@Override
	public void sortByPrice() {
		offers.sortByPrice();
	}

	@Override
	public void sortByTime() {
		offers.sortByTime();
	}
}
