package com.vova.passive.model;

import java.util.ArrayList;

public interface Model {
	ArrayList<Shipping> getOffers();
	Shipping getWithMaxPrice();
	void sortByPrice();
	void sortByTime();
}
