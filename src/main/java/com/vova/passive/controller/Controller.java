package com.vova.passive.controller;

import java.util.ArrayList;

import com.vova.passive.model.Shipping;

public interface Controller {
	ArrayList<Shipping> getOffers();
	Shipping getWithMaxPrice();
	ArrayList<Shipping> getSortedByPrice();
	ArrayList<Shipping> getSortedByTime();
}
