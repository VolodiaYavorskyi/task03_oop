package com.vova.passive.view;

@FunctionalInterface
public interface Printable {
	void print();
}
