package com.vova.passive.view;

import com.vova.passive.controller.*;
import com.vova.passive.model.Shipping;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

	private Controller controller;
	private Map<String, String> menu;
	private Map<String, Printable> methodsMenu;
	private static Scanner input = new Scanner(System.in);

	public MyView(Controller controller) {
		this.controller = controller;
		menu = new LinkedHashMap<>();
		menu.put("1", "  1 - print offers");
		menu.put("2", "  2 - print offer with max price");
		menu.put("3", "  3 - sort offers by price & print");
		menu.put("4", "  4 - sort offers by time & print");
		menu.put("Q", "  Q - exit");

		methodsMenu = new LinkedHashMap<>();
		methodsMenu.put("1", this::pressButton1);
		methodsMenu.put("2", this::pressButton2);
		methodsMenu.put("3", this::pressButton3);
		methodsMenu.put("4", this::pressButton4);
	}

	private void pressButton1() {
		for (Shipping s : controller.getOffers()) {
			System.out.println(s);
		}
	}

	private void pressButton2() {
		System.out.println(controller.getWithMaxPrice());
	}

	private void pressButton3() {
		for (Shipping s : controller.getSortedByPrice()) {
			System.out.println(s);
		}
	}

	private void pressButton4() {
		for (Shipping s : controller.getSortedByTime()) {
			System.out.println(s);
		}
	}

	//-------------------------------------------------------------------------

	private void outputMenu() {
		System.out.println("\nMENU:");
		for (String str : menu.values()) {
			System.out.println(str);
		}
	}

	public void show() {
		String keyMenu;
		do {
			outputMenu();
			System.out.println("Please, select menu point.");
			keyMenu = input.nextLine().toUpperCase();
			try {
				methodsMenu.get(keyMenu).print();
			} catch (Exception e) {
			}
		} while (!keyMenu.equals("Q"));
	}
}
